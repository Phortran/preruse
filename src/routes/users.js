const express = require('express');
const router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.render("user", { title: "Profile", userProfile: { nickname: "Sheriff" } });
});

module.exports = router;