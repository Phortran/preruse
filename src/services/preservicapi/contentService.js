const axios = require('axios');
const util = require('util');
const authenticationService = require('./authenticationService');
const properties = require('../../../src/config/propertiesLoader');

const axiosRequest = axios.create({
    baseURL: properties.loadPreservicaURL() + '/content'
});

async function getFoldersByName(folderName) {
    let url = '/search?start=0&max=10&metadata=xip.title,xip.description';

    await setHeaders();

    let body = 'q={\n' +
        '   "fields":\n' +
        '      [\n' +
        '         {"name":"xip.title","values":["' + folderName + '"]},\n' +
        '         {"name":"xip.document_type","values":["SO"]}\n' +
        '      ]\n' +
        '}';

    return await postResponse(url, body, adaptFolderByNameResponse);
}

async function searchPicturesInFolder(folderRef) {
    let url = '/search-within?parenthierarchy=' + folderRef + '&start=0&max=100&metadata=';

    await setHeaders();

    let body = 'q={\n' +
        '   "fields":\n' +
        '      [\n' +
        '         {"name":"xip.content_type_r_Display","values":["image"]},\n' +
        '         {"name":"xip.document_type","values":[IO]}\n' +
        '      ]\n' +
        '}';

    return await postResponse(url, body, adaptPictureInFolderResponse);
}

async function downloadAssets(ioRefs) {
    let url = '/download?id=';
    let promises = [];
    let errHandler = function (ref) {
        console.error('Couldn\' download asset with reference \'' + ref + '\'');
    };

    ioRefs.forEach(ioRef => {
        promises.push(new Promise(function (resolve, reject) {
            getAsset(url + ioRef, resolve, () => reject(ioRef));
        }))
    });

    let promise = Promise.all(promises.map(p => p.catch(errHandler)));
    let bitstreams = [];

    await promise
        .then(function (responses) {
            responses.forEach(function(bitstream64) {
                bitstreams.push(bitstream64);
            });
        })
        .catch(function (error) {
            // Generic error
            console.error(error);
        });

    return bitstreams;
}

async function setHeaders() {
    axiosRequest.defaults.headers.common['Preservica-Access-Token'] = await authenticationService.getAuthToken();
    axiosRequest.defaults.headers.common['Content-Type'] = 'application/x-www-form-urlencoded';
}

async function getAsset(url, resolve, reject) {
    let response;

    try {
        response = await axiosRequest.get(url, { responseType: 'arraybuffer' });

        if (response.status === 200) {
            return resolve(Buffer.from(response.data, 'binary').toString('base64'));
        } else {
            console.error(util.format('Error from Preservica. Status: %s, message: %s', response.status));
            reject();
        }
    } catch (error) {
        console.error('ERROR! - ' + error);
        reject();
    }
}

async function postResponse(url, body, callback) {
    let response;

    try {
        response = await axiosRequest.post(url, body);

        if (response.status === 200 && response.data.success === true) {
            return callback(response.data);
        } else {
            console.error(util.format('Error from Preservica. Status: %s, message: %s', response.status, response.data.success));
        }
    } catch (error) {
        console.log('ERROR! - ' + error);
    }
}

function adaptFolderByNameResponse(data) {
    // 0 - xip.title
    // 1 - xip.description
    return data.value.metadata.map((value, index) => {
        return {
            soRef: parseRef(data.value.objectIds[index]),
            folderName: value[0].value,
            description: value[1].value
        }
    });
}

function parseRef(ref) {
    return ref.substring(ref.indexOf('|') + 1);
}

function adaptPictureInFolderResponse(data) {
    // 0 - entity.ref
    return data.value.objectIds;
}

module.exports = {
    getFoldersByName,
    searchPicturesInFolder,
    downloadAssets
};