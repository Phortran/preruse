const axios = require('axios');
const util = require('util');
const properties = require('../../../src/config/propertiesLoader');

const axiosRequest = axios.create({
    baseURL: properties.loadPreservicaURL() + '/accesstoken'
});

let token = null;
let validUntil = null;
let refreshToken = null;

/**
 * Return a valid token to authenticate against Preservica for API calls, or undefined if not available
 *
 * @return a token or undefined if an error occurred
 */
async function getAuthToken() {
    if (token == null || isTokenExpired()) {
        await getTokenResponseFromPreservica();
    } else if (isTokenExpiringSoon()) {
        await refreshTokenFromPreservica();
    }

    return token;
}

function forceRefresh() {
    // set the token as expired
    validUntil = Date.now();
}

async function revokeToken() {
    if (token != null) {

    }
}

function isTokenExpired() {
    // return true if validUntil is not set at all
    return validUntil == null || validUntil - Date.now() < 0;
}

function isTokenExpiringSoon() {
    // return true if validUntil is not set at all
    return validUntil - Date.now() < (60 * 1000); // soon <- one minute
}

async function getTokenResponseFromPreservica() {
    let url = '/login?username=' + properties.loadProperty('preservica.user') +
        '&password=' + properties.loadProperty('preservica.password') +
        '&tenant=' + properties.loadProperty('preservica.tenant');

    try {
        const response = await axiosRequest.post(url);

        if (response.status === 200) {
            setAccessDetails(response.data);
        } else {
            logError(util.format('Couldn\'t get access token from Preservica; HTTP error: %s (%s)', response.status, response.statusText));
        }
    } catch (error) {
        logError('ERROR! - ' + error);
    }
}

async function refreshTokenFromPreservica() {
    let url = '/refresh?refreshToken=' + refreshToken;

    axiosRequest.defaults.headers.common['Preservica-Access-Token'] = token;

    try {
        const response = await axiosRequest.post(url);

        if (response.status === 200) {
            setAccessDetails(response.data);
        } else {
            logError(util.format('Couldn\'t refresh access token from Preservica; HTTP error: %s (%s)', response.status, response.statusText));
        }
    } catch (error) {
        logError('ERROR! - ' + error);
    }
}

async function revokeTokenFromPreservica() {
    let url = '/revoke?access-token=' + token;

    axiosRequest.defaults.headers.common['Preservica-Access-Token'] = token;

    try {
        const response = await axiosRequest.post(url);

        if (response.status !== 200) {
            logWarning(util.format('Couldn\'t revoke access token from Preservica; HTTP error: %s (%s)', response.status, response.statusText));
        }
    } catch (error) {
        logError('ERROR! - ' + error);
    } finally {
        unset();
    }
}

function setAccessDetails(responseData) {
    if (responseData.success) {
        token = responseData['token'];
        // valid until now + validFor minutes
        validUntil = Date.now() + responseData['validFor'] * 60 * 1000;
        refreshToken = responseData['refresh-token'];
    } else {
        logError('Preservica server-side error: couldn\'t send an access token');
    }

    console.info('SUCCESS - ' + token);
}

function logWarning(message) {
    console.warn(message);
}

function logError(message) {
    console.error(message);
    unset();
}

function unset() {
    token = null;
    refreshToken = null;
    validUntil = null;
}

module.exports = {
    getAuthToken,
    forceRefresh,
    revokeTokenFromPreservica
};