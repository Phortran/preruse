const {describe} = require('mocha');
const {expect} = require('chai');

var propertiesLoader = require('../../../src/config/propertiesLoader');

describe('Properties Loader unit tests:', function() {
    before(function() {
        propertiesLoader.setProperties('test/unit/config/test.properties');
    });

    it('should get right property', function() {
        expect(propertiesLoader.loadPreservicaURL()).to.equal('https://fake.preservica.co.uk/api');
    });

    it('should get right boolean', function() {
        expect(propertiesLoader.loadProperty('section1.boolean.value')).to.be.true;
    });
});