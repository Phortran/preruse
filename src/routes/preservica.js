const express = require('express');
const router = express.Router();

const contentService = require('../services/preservicapi/contentService');

router.get('/searchFolderByName', async function (req, res) {
    let folderName = req.query.folderName;

    let folderList;
    if (folderName) {
        folderList = await contentService.getFoldersByName(folderName);

        if (folderList.length === 0) {
            folderList = 'No folders found with this name!';
        }
    }

    res.send(folderList);
});

router.get('/downloadPicturesInFolder', async function (req, res) {
    let folderRef = req.query.folderRef;

    let ioRefList;
    if (folderRef) {
        ioRefList = await contentService.searchPicturesInFolder(folderRef);
    } else {
        res.send('No folder reference passed!');
        return;
    }

    let bitstreams;
    if (ioRefList.length !== 0) {
        bitstreams = await contentService.downloadAssets(ioRefList);
    } else {
        res.send('No pictures found in this folder!');
        return;
    }

    res.send(bitstreams);
});

module.exports = router;