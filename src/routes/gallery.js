const express = require('express');
const router = express.Router();

/* Gallery page. */
router.get('/', function (req, res) {
    res.render('gallery', {
        title: 'Preruse: Gallery',
        banner: 'An image gallery from a folder of your choice!'
    });
});

module.exports = router;