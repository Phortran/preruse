const {describe} = require('mocha');
const {expect, should} = require('chai');

const authenticationService = require('../../../../src/services/preservicapi/authenticationService');

describe('Authentication Service integration tests:', function () {
    before(function () {
        process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
    });

    after(async function () {
        await authenticationService.revokeTokenFromPreservica();
    });

    it('should be able to authenticate', async function () {
        await authenticationService.getAuthToken().then(token => {
            return expect(token).to.not.be.null;
        });
    });

    it('should be able to authenticate and cache token', async function () {
        this.timeout(0);
        let token = undefined;
        await authenticationService.getAuthToken().then(obtainedToken => {
            token = obtainedToken;
            return expect(obtainedToken).to.not.be.null;
        });

        await authenticationService.getAuthToken().then(obtainedToken => {
            return expect(obtainedToken).to.equal(token);
        });
    });

    it('should be able to authenticate and refresh token', async function () {
        this.timeout(0);
        let token = undefined;
        await authenticationService.getAuthToken().then(obtainedToken => {
            token = obtainedToken;
            return expect(obtainedToken).to.not.be.undefined;
        });

        authenticationService.forceRefresh();

        await authenticationService.getAuthToken().then(obtainedToken => {
            return expect(obtainedToken).to.not.equal(token);
        });
    });
});