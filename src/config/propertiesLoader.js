const PropertiesReader = require('properties-reader');
let properties = PropertiesReader('src/config/local.properties');

function loadProperty(fqdn) {
    return properties.get(fqdn);
}

function loadPreservicaURL() {
    return properties.get('preservica.URL');
}

function setProperties(path) {
    properties = PropertiesReader(path);
}

module.exports = {
    loadProperty,
    loadPreservicaURL,
    setProperties
};