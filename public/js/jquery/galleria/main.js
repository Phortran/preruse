function loadGalleria() {
    Galleria.loadTheme('/js/galleria-1.6.1/themes/twelve/galleria.twelve.min.js');
    Galleria.run('.Galleria');
}

function searchListener() {
    const source = $('#search-results').html();
    const folderTableTemplate = Handlebars.compile(source);
    let $results = $('#results');

    const parameters = {folderName: $('#search_folder').val()};

    if (parameters.folderName) {
        $results.css('display', 'block')
            .html('<img src="/images/loading.gif" width="128px" alt="loading...">');

        $.get('/preservica/searchFolderByName', parameters, function (data) {
            if (data instanceof Array) {
                $results.css('display', 'block')
                    .html(folderTableTemplate({resultsArray: data}));
            } else {
                $results.css('display', 'block')
                    .html(data);
            }
        });
    } else {
        $results.css('display', 'block')
            .html('You didn\'t give me a folder name!');
    }
}

function makeGalleryFromFolder(folderRef) {
    const parameters = {folderRef: folderRef};

    $.get('/preservica/downloadPicturesInFolder', parameters, function (bitstreams) {
        if (bitstreams instanceof Array) {
            var imgData = bitstreams.map(b => {return {data: 'data:image/jpeg;base64,' + b}});

            const source = $('#show-gallery').html();
            const dataTemplate = Handlebars.compile(source);
            let $gallery = $('#gallery-div');
            $gallery.html(dataTemplate({bitstreamArray: imgData}));

            loadGalleria();
        } else {
            console.log('error!! : ' + bitstreams);
        }
    });
}

// set a listener on the 'Enter' button to call the same function
$(function () {
    $('#search_folder').on('keyup', function (e) {
        if (e.keyCode === 13) {
            searchListener();
        }
    });
});