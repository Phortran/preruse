const {describe} = require('mocha');
const {expect, should} = require('chai');

const contentService = require('../../../../src/services/preservicapi/contentService');
const authenticationService = require('../../../../src/services/preservicapi/authenticationService');

describe('Content API module integration tests:', function () {
    before(function () {
        process.env["NODE_TLS_REJECT_UNAUTHORIZED"] = 0;
    });

    after(async function () {
        await authenticationService.revokeTokenFromPreservica();
    });

    it('should be able to list existing folder', async function () {
        this.timeout(0);
        let folders = await contentService.getFoldersByName('Cars');

        expect(folders.length).to.equal(2);
        expect(folders[0]).to.deep.equal({folderName: 'Cars', description: 'A folder with pictures of cars'});
        expect(folders[1]).to.deep.equal({folderName: 'German Cars', description: 'Cars from Germany exclusively'});
    });

    // it('should be able to authenticate and cache token', async function () {
    //     this.timeout(0);
    //     let token = undefined;
    //     await authenticator.getAuthToken().then(obtainedToken => {
    //         token = obtainedToken;
    //         return expect(obtainedToken).to.not.be.null;
    //     });
    //
    //     await authenticator.getAuthToken().then(obtainedToken => {
    //         return expect(obtainedToken).to.equal(token);
    //     });
    // });
    //
    // it('should be able to authenticate and refresh token', async function () {
    //     this.timeout(0);
    //     let token = undefined;
    //     await authenticator.getAuthToken().then(obtainedToken => {
    //         token = obtainedToken;
    //         return expect(obtainedToken).to.not.be.undefined;
    //     });
    //
    //     authenticator.forceRefresh();
    //
    //     await authenticator.getAuthToken().then(obtainedToken => {
    //         return expect(obtainedToken).to.not.equal(token);
    //     });
    // });
});